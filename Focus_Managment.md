# Focus Management

### Q1. What is Deep Work?
Deep work is the capacity to concentrate on a mentally taxing task without being distracted. It is a necessary talent for success in the field of knowledge work.

### Q2. According to author how to do deep work properly, in a few points?
* Look for a peaceful workspace.
* Disable the notifications on your computer and phone.
* Work uninterrupted for twenty-five minutes after setting a timer.
* To prevent burnout, take breaks every 25 minutes.


### Q3. How can you implement the principles in your day to day life?
* To begin with, let's look for a peaceful workspace.
* Disabling notifications on your PC and phone.
* Set a 25-minute timer and work uninterrupted during that period.
* In order to prevent burnout, take a break every 25 minutes.

### Q4. What are the dangers of social media, in brief?
Social media can be harmful because it can cause worry, impair mental health, decrease focus, and disseminate false information and fake news. Before utilizing social media, one should take into account these very real risks.
  

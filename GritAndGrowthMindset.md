### Question 1
## Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.
*In a video, Angela Duckworth discusses the importance of passion and perseverance in achieving academic and personal success. She discovered that perseverance and the idea that life is a marathon rather than a sprint determine success, and that students who are more resilient and don't give up easily are more likely to complete their education.*


### Question 2
## Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.
*Growth mindset, according to Professor Carol Dweck in a video, is the belief that you can get smarter by working hard. It aids in your improvement and learning. Even though you may feel helpless at times, realizing this allows you to grow and adapt. It's not the same as having a fixed mindset, which holds that your skills are set in stone and cannot be altered.*



### Question 3
## What is the Internal Locus of Control? What is the key point in the video?
*The idea that one has control over their own life and results is known as the Internal Locus of Control. According to the Columbia University study depicted in the movie, fifth graders who believed that hard work was the key to success concentrated on simpler assignments and were less dedicated to more challenging ones. Conversely, individuals who believed that intelligence was the key to success were more driven and tenacious. The video's advise on developing an internal center of control to maintain motivation is its main selling point.*

### Question 4
## What are the key points mentioned by speaker to build growth mindset.
* Have self-confidence
* Examine your presumptions
* Create a curriculum for your dreams
* Learn from your mistakes 
* Respect the struggle.


### Question 5
## What are your ideas to take action and build Growth Mindset?
*I believe in myself. I think descipline mindset, learn from failure and never give up attitude can 
make and Build Growth Mindset.*


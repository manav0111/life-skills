### Question 1
## In this video, what was the most interesting story or idea for you?
*Starting small with tiny habits is a better course of action here than trying major changes all at once. Habits are made up of three components: the routine (what you do), the reward (what you get), and the cue (what initiates it). You can establish healthy habits more readily by making even a small adjustment to one of these. It works well for forming habits.*


### Question 2
## How can you use B = MAP to make making new habits easier? What are M, A and P.
*Behavior equals Motivation, Ability, and Prompt, or B = MAP, as it is stated in the BJ Fogg "Tiny Habits Method" video. Should I wish to begin a new routine, such as working out. Here's a little tip: Consider B = MAP. 'B' is how I behave, such as when I work out. Motivation is the letter "M": why do I want to work out? The more lucid my rationale, the better. "A" stands for ability; make it very simple, akin to a quick workout. And last, 'P' is for prompt. Set a reminder or associate it with a habit I already have, like working out after brushing my teeth. It's much simpler to create new habits when you follow B = MAP.*


### Question 3
## Why it is important to "Shine" or Celebrate after each successful completion of habit?
*Rewarding yourself for completing a habit is crucial since it reinforces the habit in your brain. You're more inclined to stick with something when it makes you feel good. According to BJ Fogg, celebrating every little victory develops into a habit. Adding a happy experience so helps a habit persist by signaling to your brain, "Hey, this is good!" after performing the behavior.*


### Question 4
## In this video, what was the most interesting story or idea for you?
*The idea of improving everything you do by just 1% is the most intriguing one here. It's similar to how a cycling team improved dramatically by making numerous tiny adjustments. This demonstrates how little routines and advancements over time can build up to something truly significant. The lesson here is to never undervalue the significance of tiny, steady steps—they have the potential to lead to great achievement!*



### Question 5
## What is the book's perspective about Identity?
*"Atomic Habits" suggests that if you want positive habits to endure, consider becoming someone who demonstrates those behaviors. Making routines a part of who you are is more important than merely achieving your goals. Gradual adjustments build up over time, so concentrate on maintaining consistency and developing a positive identity around your routines.*


### Question 6
## Write about the book's perspective on how to make a habit easier to do?
*In order to simplify habits, the book "Atomic Habits" proposes segmenting them into four stages: cue, craving, response, and reward. It places a strong emphasis on fostering an atmosphere that makes routines appealing, sating cravings right away, and making sure they are simple to initiate and maintain. Simplifying and making habits fun is the objective here, as it increases the chances of effective and regular habit formation.*


### Question 7
## Write about the book's perspective on how to make a habit harder to do?
*While the book "Atomic Habits" doesn't explicitly address making bad habits harder, it does recommend concentrating on setting up conditions that make bad habits more difficult and good habits simpler. For example, you may put harmful items out of reach if you wish to quit munching. The goal is to create an environment that supports positive habits and makes negative ones more difficult.*

### Question 8:
## Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
**Habit: Consistent Coding Practice**

   * Cue (When to Start):
         *Set a daily coding time.
         *Keep coding environment ready and easily accessible.

    * Attractiveness (Make it Fun):
        *Work on coding projects that align with personal interests.
        *Explore new technologies or languages that spark curiosity.
        

    * Ease (Keep it Simple):
        *Begin with short coding sessions to avoid feeling overwhelmed.
        *Break coding tasks into manageable parts or small projects.

    * Satisfaction (Feel Good About It):
        *Celebrate small coding achievements, such as solving a problem.

### Question 9:
## Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
**Choosing Nutritious Snacks Over Unhealthy Options**

    *Invisibility (Hide the Cue):
        *Remove unhealthy snacks from visible areas in the home.
    
    *Unattractiveness (Make it Not Fun):
        *Educate yourself about the nutritional benefits of healthier snack choices.
       
    *Difficulty (Make it Hard):
        *Plan and prepare a variety of healthy snacks in advance.
        


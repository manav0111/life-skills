## What is the Feynman Technique? Explain in 1 line.
First Principle: Don't fool yourself
* Step 1: Take a piece of paper and write the concept's name at the top
* Step 2: Explain the concept using simple language
* Step 3: Identify problem areas, then go back to the sources to review
* Step 4: Pinpoint any complicated terms and challenge yourself to simplify them


## In this video, what was the most interesting story or idea for you?(Learning How to Learn TED talk by Barbara Oakley)
In her TED talk, Barbara Oakley discusses her struggles with math and science and how she overcame them by learning effective strategies. She describes two ways of thinking about learning: diffuse and focused. She also discusses how procrastination can be overcome. Oakley introduces the Pomodoro technique, which is a technique for focused work. She also stresses the need for practice, testing, and recall in learning, emphasizing that mastery requires more than just understanding.


## What are active and diffused modes of thinking?
* **Active or Focused Thinking Style:** This is the state in which you are actively focusing on a task, such as learning a new skill or resolving an issue.
* Devoting all of your concentration to solving a math problem or learning a new subject.
* **Intense Thinking Style:** In this more at ease condition, your thoughts are allowed to roam. It's beneficial for forming imaginative connections and gaining perspective.As an illustration:
* Daydreaming or taking a break from focused attention.

## According to the video, what are the steps to take when approaching a new topic? Only mention the points.
* Divide the skill into small, doable chunks. 
* Learn enough so that you can improve on your own mistakes. Learn From mistakes
* Take down obstacles to practice.
* Put in at least 20 hours of practice.
* More practice become more perfect 


### What are some of the actions you can take going forward to improve your learning process?
* Set Clear Goals
* Active Learning
* Regular Practice
* Time Managment
* Experiment With Learning Techniques
* Stay Updated
* Rivise Previous Concepts

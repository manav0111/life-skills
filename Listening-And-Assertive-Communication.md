### Question1
## What are the steps/strategies to do Active Listening? 
* Focus on the Speaker and Topic.
* Give you full attention
* Show that you're listening
* Provide feedback
* Remember key points
* Be open Minded


### Question 2
## According to Fisher's model, what are the key points of Reflective Listening?

* **Daily Life Application:** Helpful in interpersonal relationships, especially when encouraging others to be more forthcoming about their feelings. 
* **Rooted in Empathy:** Derived from Carl Rogers' therapy, reflecting listening focuses on genuinely understanding and empathy. 
* **Confirm Understanding:** Reflective listening entails confirming the speaker's idea to ensure correct understandin


### Question 3
## What are the obstacles in your listening process?
* Lack of Concentration
* Distractions
* Prejudgment
* Selective Listening



### Question 4
## What can you do to improve your listening?
* Give full attention
* Practise active listening
* Avoid Interrupting
* Be pateint
* Show Empathy
* Continious Learning

### Question 5
## When do you switch to Passive communication style in your day to day life?
When I want to keep things amicable and steer clear of confrontations, I typically communicate in a passive manner. Even though I'm a big proponent of open communication, there are occasions when I put preserving good connections ahead of assertively voicing my opinions.


### Question 6
## When do you switch into Aggressive communication styles in your day to day life?
In my daily interactions, if I'm feeling stressed out or unhappy, I may adopt an aggressive communication style. I might speak up more vehemently or react more passionately than normal during these moments.


### Question7
## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
When I'm angry or frustrated, I often resort to passive-aggressive communication techniques like sarcasm, gossiping, taunting, or the silent treatment in my daily interactions. This usually occurs when I'm unable or reluctant to communicate my emotions honestly. 


### Question 8
## How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

* **Express My Emotions** - I'll express my feelings without pointing fingers. I'll be open about how I'm feeling.

* **Know My Needs** - I'll assess a situation to determine what I actually need. I will be explicit about it.

* **Practice in Easy Moments** - To build confidence, I'll work on speaking up in informal settings.

* **Observe My Appearance and Voice**-I'll be mindful of my tone and body language. I will ensure that they align with my words.


* **Speak Up Earlier** - I'll talk about difficulties right away. To get things fixed more quickly, I'll speak up early
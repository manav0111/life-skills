## Question 1
### Which point(s) were new to you?
**Answer**-It's fantastic to hear that you've found the suggestion to use tools like Loom for screencasts and GitHub Gists for code snippets valuable. Incorporating these tools into your workflow can indeed be a game-changer for effective communication. By leveraging visual aids, you enhance your ability to convey information, making it more engaging and easily understandable.

## Question 2
### Which area do you think you need to improve on? What are your ideas to make progress in that area?
**Answer**-There are certain technologies or tools in my domain where I need more improvement and dedeicate time for learning tools and enrolling in online courses,to improve my knowledge.



# Refactoring Legacy Code with SOLID Principles Introduction

In the ever-evolving landscape of software development, maintaining a clean and maintainable codebase is crucial for long-term success. This report focuses on the application of SOLID principles, a set of design principles that aim to enhance the robustness and maintainability of object-oriented software systems. The project at hand presents challenges in code management, prompting an exploration of SOLID principles to guide the refactoring process

## SOLID Principles
* Single Responsibility Principle (SRP)
* Open/Closed Principle (OCP)
* Liskov Substitution Principle (LSP)
* Interface Segregation Principle (ISP)
* Dependency Inversion Principle (DIP)


1-**Single Responsibility Principle (SRP)**
Each class should have only one reason to change. This avoids the scenario where a class has multiple responsibilities, making the code more prone to bugs and harder to maintain.

2- **Open/Closed Principle (OCP)**
Software entities (classes, modules, functions, etc.) should be open for extension but closed for modification. This principle encourages the use of interfaces and abstract classes to allow for future enhancements without altering existing code.

3- **Liskov Substitution Principle (LSP)**
Subtypes should be substitutable for their base types without altering the correctness of the program. In other words, objects of a superclass should be replaceable with objects of a subclass without affecting the program's functionality.

4-  **Interface Segregation Principle (ISP)**
 A class should not be forced to implement interfaces it does not use. This principle advocates for smaller, specific interfaces rather than large.

5- **Dependency Inversion Principle (DIP):**
High-level modules should not depend on low-level modules; both should depend on abstractions. Abstractions should not depend on details; details should depend on abstractions.

## Code Refactoring Examples

###  Before Refactoring  SRP-
``` javascript
class Report {
    generateReport(data) {
        // ... existing code for report generation ...
        this.saveReportToFile(data);
    }

    saveReportToFile(data) {
        // ... existing code for saving the report to a file ...
    }
}

```
In the initial code, the Report class has two responsibilities: generating a report and saving the report to a file. This violates the SRP because a change in the report generation logic might also impact the file-saving logic, and vice versa.

### After Refactoring SRP :
``` javascript
class Report {
    generateReport(data) {
        // ... existing code for report generation ...
    }
}

class ReportSaver {
    saveReportToFile(data) {
        // ... existing code for saving the report to a file ...
    }
}
```
###  Before Refactoring ISP -
```  javascript
// Before Applying ISP
class Worker {
    work() {
        // ... code for working ...
    }

    eat() {
        // ... code for eating ...
    }
}
```
###  After Refactoring ISP -
``` javascript
// After Applying ISP
class Workable {
    work() {
        // ... code for working ...
    }
}

class Eatable {
    eat() {
        // ... code for eating ...
    }
}

class Worker implements Workable, Eatable {
    // ... existing code for Worker ...
}

```

### Before Refactor OCP-
``` javascript
// Before Applying OCP
class Rectangle {
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }
}

class AreaCalculator {
    calculateArea(rectangle) {
        return rectangle.width * rectangle.height;
    }
}
```
### After Refactor OCP-

``` javascript

// After Applying OCP
class Shape {
    area() {
        throw new Error("Method 'area' must be implemented");
    }
}

class Rectangle extends Shape {
    // ... existing code for Rectangle ...
}

class Circle extends Shape {
    // ... existing code for Circle ...
}

class AreaCalculator {
    calculateArea(shape) {
        return shape.area();
    }
}
```


### Before Refactor LSP-
``` javascript
// Before Applying LSP
class Bird {
    fly() {
        console.log('Bird is flying');
    }
}

class Penguin extends Bird {
    // Penguins can't fly, but still inherit the fly method
}



```
### After Refactor LSP
``` javascript
// After Applying LSP
class Bird {
    move() {
        console.log('Bird is moving');
    }
}

class FlyingBird extends Bird {
    fly() {
        console.log('Bird is flying');
    }
}


class SwimmingBird extends Bird {
    swim() {
        console.log('Bird is swimming');
    }
}

class Penguin extends SwimmingBird {
    // Penguins can swim, and they only inherit the swim method
}
```

## REFERENCES -
[YOUTUBE](https://www.youtube.com/watch?v=hGf2upfDpdo&list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme&index=2)

[DIGITAL OCEAN](https://www.digitalocean.com/community/conceptual-articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)

## CONCLUSION -
By adhering to SOLID principles, the refactoring process  aims to create a more maintainable and extensible code base. These principles provide a foundation for scalable software development, enhancing the project's robustness and adaptability.
















# 1. What types of behaviors lead to sexual harassment?

There are two primary categories of sexual harassment.

## Quid Pro Quo (this for that):

This usually entails a person who is requesting sexual favors from someone in a position of power in exchange for incentives and work evaluations.

## Hostile Work Environment:

This happens when persistent, inappropriate, and sexual remarks cause discomfort and render the workplace as a whole intolerable.

# 2. What would you do in case you face or witness any incident or repeated incidents of such behavior?

* I will report any issues I observe to higher authorities.
* I'll let the offending party know that their actions are unacceptable.
* I'll step in to put an end to the inappropriate behavior if I'm present.
* If I were a witness, I would step in to put an end to the harassment and          determine whether the person needed assistance.
* I will try to do all Possible things which I can to help that person which 
faces sexual Harsment.

# REFERENCE-
[YOUTUBE](https://www.youtube.com/watch?v=M8C2B949BqA)
[YOUTUBE](https://www.youtube.com/watch?v=Ue3BTGW3uRQ)







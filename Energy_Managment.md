### Question 1
## What are the activities you do that make you relax - Calm quadrant?

* Deep Breathing: To relax the mind, take slow, deep breaths.

* Walks:nature to relax.

* Music: To have a relaxing effect, listen to tranquil music.


* Hot Showers and Baths: A warm bath might help you decompress.

* Journaling: To help you decompress, write down your ideas.

* Disconnect: For mental clarity, take pauses from your work.


### Question 2:
## When do you find getting into the Stress quadrant?
* Acquiring new knowledge.
* Working out vigorously.
* Dealing with challenging work or high standards.
* Handling unforeseen changes or tight timelines.
* Managing disagreements or stress

### Question 3:
## How do you understand if you are in the Excitement quadrant?
* I laugh and grin more.
* I'm focused and involved.
* My attitude remains upbeat.
* I'm bursting with energy.
* My words and deeds exhibit enthusiasm.
* I am receptive to fresh perspectives and chances.

### Question 4
## Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.
* Sleep is crucial for body and brain health.
* Lack of sleep can harm health, increase heart rates, and cause accidents.
* Sleep is essential for memory, learning, and general wellbeing. 
* Insufficient sleep impairs immunity, increasing the risk of illnesses.
* Rearranged gene activity was discovered in a study after just four hours of sleep. * Quality sleep is enhanced by regular sleep and a cold bedroom.

### Question 5
## What are some ideas that you can implement to sleep better?
I can use these tips to improve my sleep: * Setting a consistent wake-up and bedtime.
* Creating a cool, dark, and quiet bedroom.
* Avoiding screens just before bed 
* Steering clear of heavy meals and beverages right before bed.


### Question 6
## Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.
* Minimal exercise boosts mood, attention, and memory.
* Exercise brings long-lasting benefits.
* Guards against neurodegenerative diseases.
* Try to work out for 30 minutes every day, preferably with some aerobics.
* Exercise just one minute can improve mental wellness.

### Question 7
## What are some steps you can take to exercise more?
* Walk instead of drive, whenever you can.
* Take the stairs instead of the escalator or elevator.
* Take a family walk after dinner.
* Replace a Sunday drive with a Sunday walk.
* Go for a half-hour walk instead of watching TV.
* Get off the bus a stop early, and walk.
* Set realistic goals to stay motivated.
* Make it a social activity by exercising with friends or family.
